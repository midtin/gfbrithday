<html>
  <head>
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/css/custom.css" rel="stylesheet">
  </head>
  <body>
    <input id="page" type="text" value="{{ page }}" hidden/>
    <div class="row content">
      <div class="col-md-12 message">
        <p id="first-paragraph" hidden></p>
        <p id="second-paragraph" hidden></p>
        <p id="third-paragraph" hidden></p>
        <a id="top-map" hidden><span class="glyphicon glyphicon-chevron-down"></span></a>
      </div>
    </div>
    <div class="row content">
      <img id="treasure-map" src="/static/image/map-1.jpg" />
    </div>
  </body>
  <script src="/static/js/jquery-1.11.3.min.js"></script>
  <script src="/static/js/bootstrap.min.js"></script>
  <script src="/static/js/jquery.scrollTo.min.js"></script>
  <script src="/static/js/app.js"></script>
</html>
