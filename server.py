# -*- coding:utf-8 -*-
from bottle import Bottle, static_file, template

app = Bottle()


@app.get('/')
@app.get('/<page>')
def index(page=1):
  return template('mygirl', page=page)


@app.get('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root='./static/')
