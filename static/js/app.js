$(function(){
  var page = $('#page').val();
  var $imgMap = $('#treasure-map');
  var $urlMap = $('#top-map');
  var $message = $('.message');
  var $pFirst = $('#first-paragraph');
  var $pSecond = $('#second-paragraph');
  var $pThird = $('#third-paragraph');

  var urlTimeout = 8000;
  if (page === '1') {
    $pFirst.text('自2012年10月6日');
    $pSecond.text('我们一起走过1161天');
  }
  else if (page === '2'){
    $pFirst.text('愿能和猪猪一起');
    $pSecond.text('开心走过每个你我的生日');
  }
  else if (page === '3') {
    $pFirst.text('猪猪');
    $pSecond.text('生日快乐');
    $pThird.text('爱你');
    setTimeout(function(){
      $pThird.fadeIn(6000);
    }, 8000);
    urlTimeout = 12000
    $('.glyphicon-chevron-down').css('margin-top', '15%')
  };

  $pFirst.fadeIn(6000);
  setTimeout(function(){
    $pSecond.fadeIn(6000);
  }, 4000);
  setTimeout(function(){
    $urlMap.fadeIn(1000);
  }, urlTimeout);

  $imgMap.attr('src', '/static/image/map-' + page + '.jpg');

  $urlMap.on('click', function(){
    $.scrollTo('#treasure-map', 800);
  })

});
